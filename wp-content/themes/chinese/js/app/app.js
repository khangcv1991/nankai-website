var app = angular.module('education', ['ngRoute']);
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "wp-content/themes/chinese/partials/scene.html"
        })
        .when("/scene", {
            templateUrl: "wp-content/themes/chinese/partials/scene.html"
        })
        .when("/dialog", {
            templateUrl: "wp-content/themes/chinese/partials/dialog.html"
        })
        .when("/sentence", {
            templateUrl: "wp-content/themes/chinese/partials/sentence.html"
        });
});

app.controller('eduController', function ($scope, $http, $location, $window, player) {

    $scope.main_menu = MENU_ITEMS;
    $scope.app_copyright = undefined;
    $scope.content_copyright = undefined;

    $scope.categories = undefined;
    $scope.locations = undefined;
    $scope.scenes = [];
    $scope.dialogs = [];
    $scope.sentences = [];

    $scope.selDialog = {id: undefined, sequence: undefined};
    $scope.selScene = {id: undefined, sequence: undefined};
    $scope.link = {audios: {en: undefined, ch_hz: undefined}, videos: {en: undefined, ch_hz: undefined}};
    $scope.version;
    $scope.app_build = undefined;


    $scope.sentence_modes = undefined;
    $scope.localisation = undefined;
    $scope.category_location = undefined;
    $scope.about_navigation = undefined;
    $scope.about_data = undefined;

    $scope.flag = {
        scene: true,
        dialog: false,
        sentence: false,
        eng: false,
        hz_eng: false,
        hz_py: true,
        hz: false
    }
    $scope.localisation = {
        eng: false,
        hz: true
    }
    //init
    init();
    //end


    $scope.listScenes = function () {
        displayNavigationItems(1);
        $http.get(BASE_URL + 'scenes').then(function (response) {
            $scope.scenes = response.data;

        });
        $scope.selDialog = {id: undefined, sequence: undefined};
        $scope.selScene = {id: undefined, sequence: undefined};
    }
    $scope.listDialogsById = function (scene) {

        $scope.selScene = scene;

        console.log($scope.selScene);

        getVideoLinkBySceneId(scene.id);

        $scope.dialogs.length = 0;
        displayNavigationItems(2);
        console.log(BASE_URL + 'dialogs/' + scene.id);
        $http.get(BASE_URL + 'dialogs/' + scene.id).then(function (response) {
            $scope.dialogs = response.data.dialogs;


        });
        $scope.selDialog = {id: undefined, sequence: undefined};


    }


    $scope.listSentencesById = function (dialog) {
        $scope.selDialog.id = dialog.id;
        $scope.selDialog.sequence = dialog.sequence
        console.log($scope.selDialog);
        getAudioLinkByDialogId(dialog.id);

        $scope.sentences.length = 0;
        displayNavigationItems(3);
        console.log(BASE_URL + 'sentences/' + dialog.id);
        $http.get(BASE_URL + 'sentences/' + dialog.id).then(function (response) {
            $scope.sentences = response.data;
        });
    }

    $scope.listScenesByCategoryId = function (id) {
        displayNavigationItems(1);
        $http.get(BASE_URL + 'categories/' + id).then(function (response) {
            $scope.scenes = response.data;

        });
    }
    $scope.listScenesByLocationId = function (id){
        displayNavigationItems(1);
        $http.get(BASE_URL + 'locations/' + id).then(function (response) {
            $scope.scenes = response.data;

        });
    }

    function init() {
        $scope.main_menu = MENU_ITEMS;
        $scope.app_copyright = APP_COPYRIGHT;
        $scope.content_copyright = CONTENT_COPYRIGHT;
        $scope.sentence_modes = SENTENCE_MODES;
        $scope.localisation = LOCALISATION;
        $scope.category_location = CATEGORY_LOCATION;
        $scope.about_navigation = ABOUT;
        $scope.app_build = APP_BUILD;
        console.log("version: " + $scope.app_build.version_no + " -- " + $scope.app_build.git_no);

        setBrowserLocalisation();
        displayCategory_Location(1);
        displayAbout(1);

        $location.path('scene');
        displayNavigationItems(1);
        $http.get(BASE_URL + 'scenes').then(function (response) {
            $scope.scenes = response.data;

        });


        $http.get(BASE_URL + 'version').then(function (response) {
            $scope.version = response.data;
        });
        getCategories();

        getLocations();
        getAbout();
    }

    function getAbout() {

        if ($scope.about_data == undefined) {
            $scope.about_data = {};
            $http.get(BASE_URL + 'about').then(function (response) {
                response.data.forEach(function (item) {
                    $scope.about_data[item.key] = item.value;
                });
                console.log($scope.about_data);
            });
        }
    }

    function getCategories() {
        if ($scope.categories == undefined || $scope.categories.length != 0) {
            $http.get(BASE_URL + 'categories').then(function (response) {
                $scope.categories = response.data;

            });
        }
    }

    function getLocations() {
        if ($scope.locations == undefined || $scope.locations.length != 0) {
            $http.get(BASE_URL + 'locations').then(function (response) {
                $scope.locations = response.data;
                // $scope.locations =  removeDuplicates($scope.locations, "nameTranslations.en");

            });
        }
    }

    function getVideoLinkBySceneId(id) {

        var STRING_VIDEO = MEDIA_LINK + 'v{0}/app/nankai/media/scene00{1}/video-s00{2}-{3}.mp4';

        if (parseInt($scope.selScene.sequence) > 10) {
            STRING_VIDEO = MEDIA_LINK + 'v{0}/app/nankai/media/scene0{1}/video-s0{2}-{3}.mp4';
        }

        $scope.link.videos.ch_hz = STRING_VIDEO.format($scope.version.version_no, $scope.selScene.sequence, $scope.selScene.sequence, 'cn')
        $scope.link.videos.en = STRING_VIDEO.format($scope.version.version_no, $scope.selScene.sequence, $scope.selScene.sequence, 'en')

        console.log($scope.link.videos);

    }

    function getAudioLinkByDialogId(id) {

        var STRING_AUDIO = MEDIA_LINK + 'v{0}/app/nankai/media/scene00{1}/audio-s00{2}-d00{3}-{4}.mp3';
        if ($scope.selScene.sequence > 10 && $scope.selDialog.sequence < 10)
            STRING_AUDIO = MEDIA_LINK + 'v{0}/app/nankai/media/scene0{1}/audio-s0{2}-d00{3}-{4}.mp3';
        if ($scope.selScene.sequence > 10 && $scope.selDialog.sequence > 10)
            STRING_AUDIO = MEDIA_LINK + 'v{0}/app/nankai/media/scene0{1}/audio-s0{2}-d0{3}-{4}.mp3';
        if ($scope.selScene.sequence < 10 && $scope.selDialog.sequence > 10)
            STRING_AUDIO = MEDIA_LINK + 'v{0}/app/nankai/media/scene00{1}/audio-s0{2}-d0{3}-{4}.mp3';


        $scope.link.audios.ch_hz = STRING_AUDIO.format($scope.version.version_no, parseInt($scope.selScene.sequence), parseInt($scope.selScene.sequence), parseInt($scope.selDialog.sequence), 'cn');
        $scope.link.audios.ch_hz = STRING_AUDIO.format($scope.version.version_no, parseInt($scope.selScene.sequence), parseInt($scope.selScene.sequence), parseInt($scope.selDialog.sequence), 'en');


        console.log($scope.link.audios);

    }

    /*****************navigation and radia button ***************/
    $scope.displayAbout = displayAbout;
    //opt : 1 - author
    //opt : 2 - app
    function displayAbout(opt) {
        if (opt == 1) {
            ABOUT.professor = true;
            ABOUT.app = false;
        }
        if (opt == 2) {
            ABOUT.professor = false;
            ABOUT.app = true;
        }
    }


    $scope.displayCategory_Location = displayCategory_Location;
    //opt : 1 - category
    //opt : 2 - location
    function displayCategory_Location(opt) {
        if (opt == 1) {
            CATEGORY_LOCATION.sel_category = true;
            CATEGORY_LOCATION.sel_location = false;
        }
        if (opt == 2) {
            CATEGORY_LOCATION.sel_category = false;
            CATEGORY_LOCATION.sel_location = true;
        }
    }

    //flag : 1  - scene
    //flag : 2 - scene - dialog
    //flag: 3 - scene - dialog -sentence
    function displayNavigationItems(flag) {
        if (flag == 1) {
            $scope.flag.scene = true;
            $scope.flag.dialog = false;
            $scope.flag.sentence = false;

        }
        if (flag == 2) {
            $scope.flag.scene = true;
            $scope.flag.dialog = true;
            $scope.flag.sentence = false;

        }

        if (flag == 3) {
            $scope.flag.scene = true;
            $scope.flag.dialog = true;
            $scope.flag.sentence = true;

        }

    }

    $scope.displaySentencesMode = displaySentencesByOptions;
    /***
     * 1: hz_py
     * 2:hz_eng
     * 3:eng
     * 4:hz

     */

    $scope.style_css = {hz_py: '', eng: ''};

    var style_code = "{ border-bottom-color: rgb(255, 51, 34) !important; border-bottom: 4px solid; }";

    function displaySentencesByOptions(opt) {

        if (opt == 1) {
            $scope.flag.hz_py = true;
            $scope.flag.hz_eng = false;
            $scope.flag.eng = false;
            $scope.flag.hz = false;

            $scope.style_css.hz_py = style_code;
            $scope.style_css.eng = '{font-size: small; cursor:pointer;}';

        }
        if (opt == 2) {
            $scope.flag.hz_py = false;
            $scope.flag.hz_eng = true;
            $scope.flag.eng = false;
            $scope.flag.hz = false;

        }

        if (opt == 3) {
            $scope.flag.hz_py = false;
            $scope.flag.hz_eng = false;
            $scope.flag.eng = true;
            $scope.flag.hz = false;

            $scope.style_css.hz_py = '{font-size: small; cursor:pointer;}';
            $scope.style_css.eng = style_code;
        }
        if (opt == 4) {

            $scope.flag.hz_py = false;
            $scope.flag.hz_eng = false;
            $scope.flag.eng = false;
            $scope.flag.hz = true;
        }
    }


    /*****************localisation ***************/
    $scope.displayContentByLocalisation = displayContentByLocalisation;

    /**
     *
     * @param opt: 1 - eng
     *              2- hz
     */
    function displayContentByLocalisation(opt) {

        if (opt == 1) {
            $scope.localisation.eng = true;
            $scope.localisation.hz = false;


        }
        if (opt == 2) {
            $scope.localisation.eng = false;
            $scope.localisation.hz = true;

        }
    }

    function setBrowserLocalisation() {
        var lang = $window.navigator.language || $window.navigator.userLanguage;
        if (lang.includes('zh')) {
            $scope.localisation.hz = true;
            $scope.localisation.eng = false;

            $scope.flag.hz_py = false;
            $scope.flag.eng = true;

        } else {
            $scope.localisation.hz = false;
            $scope.localisation.eng = true;


            $scope.flag.hz_py = true;
            $scope.flag.eng = false;
        }
    }

    /*****************scene, dialog navigation ***************/

    $scope.previousScene = function () {

        if ($scope.selScene.sequence == 1) {
            $scope.selScene = $scope.scenes[$scope.scenes.length - 1];
        } else {
            $scope.selScene = $scope.scenes[$scope.selScene.sequence - 2];
        }
        $scope.listDialogsById($scope.selScene);
        $location.path('dialog');
    }
    $scope.nextScene = function () {

        if ($scope.scenes.length == $scope.selScene.sequence) {
            $scope.selScene = $scope.scenes[0];
        } else {
            $scope.selScene = $scope.scenes[$scope.selScene.sequence];
        }

        $scope.listDialogsById($scope.selScene);
        $location.path('dialog');
    }

    $scope.previousDialog = function () {
        if ($scope.selDialog.sequence == 1) {
            $scope.selDialog = $scope.dialogs[$scope.dialogs.length - 1];
        } else {
            $scope.selDialog = $scope.dialogs[$scope.selDialog.sequence - 2];
        }

        $scope.listSentencesById($scope.selDialog);

        $location.path('sentence');
    }
    $scope.nextDialog = function () {

        if ($scope.dialogs.length == $scope.selDialog.sequence) {
            $scope.selDialog = $scope.dialogs[0];
        } else {
            $scope.selDialog = $scope.dialogs[$scope.selDialog.sequence];
        }

        $scope.listSentencesById($scope.selDialog);
        $location.path('sentence');
    }


});
app.factory('player', function (audio, $rootScope) {
    var player,
        playlist = [],
        paused = false,
        current = {
            album: 0,
            track: 0
        };

    player = {
        playlist: playlist,

        current: current,

        playing: false,

        play: function (track, album) {
            if (!playlist.length) return;

            if (angular.isDefined(track)) current.track = track;
            if (angular.isDefined(album)) current.album = album;

            if (!paused) audio.src = playlist[current.album].tracks[current.track].url;
            audio.play();
            player.playing = true;
            paused = false;
        },

        pause: function () {
            if (player.playing) {
                audio.pause();
                player.playing = false;
                paused = true;
            }
        },

        reset: function () {
            player.pause();
            current.album = 0;
            current.track = 0;
        },

        next: function () {
            if (!playlist.length) return;
            paused = false;
            if (playlist[current.album].tracks.length > (current.track + 1)) {
                current.track++;
            } else {
                current.track = 0;
                current.album = (current.album + 1) % playlist.length;
            }
            if (player.playing) player.play();
        },

        previous: function () {
            if (!playlist.length) return;
            paused = false;
            if (current.track > 0) {
                current.track--;
            } else {
                current.album = (current.album - 1 + playlist.length) % playlist.length;
                current.track = playlist[current.album].tracks.length - 1;
            }
            if (player.playing) player.play();
        }
    };

    playlist.add = function (album) {
        if (playlist.indexOf(album) != -1) return;
        playlist.push(album);
    };

    playlist.remove = function (album) {
        var index = playlist.indexOf(album);
        if (index == current.album) player.reset();
        playlist.splice(index, 1);
    };

    audio.addEventListener('ended', function () {
        $rootScope.$apply(player.next);
    }, false);

    return player;
});


// extract the audio for making the player easier to test
app.factory('audio', function ($document) {
    var audio = $document[0].createElement('audio');
    return audio;
});