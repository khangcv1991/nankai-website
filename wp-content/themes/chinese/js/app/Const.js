/**
 * Created by admin on 25/8/17.
 */
const BASE_URL = "http://128.199.213.219:8070/api/";
const MEDIA_LINK = "http://cdn.jinjiyingyu.com:8880/";

const APP_COPYRIGHT = "All Content Copyright © Zhou Wen Zhuang and Nankai University Press";
const CONTENT_COPYRIGHT = "App Copyright © Infotecture Development Pty Ltd";
const APP_BUILD = {
    version_no: '1.0 build 1.0.122eab6-20170912',
    git_no: '69359ca8'
};

const MENU_ITEMS = [
    {
        eng: 'HOME',
        hz: '首页',
        link: '#scene',
        action: 'listScenes();'

    },
    {
        eng: 'CATEGORIES',
        hz: '类别',
        link: '',
        sub_menus: []
    },
    {
        eng: 'LOCATIONS',
        hz: '地点',
        link: ''

    },
    {
        eng: 'NEWS',
        hz: '新闻',
        link: ''
    },
    {
        eng: 'WIKI',
        hz: 'WIKI',
        link: ''
    },
    {
        eng: 'ABOUT',
        hz: '关于',
        link: ''
    }
];

const CATEGORY_LOCATION = [
    {sel_category: true},
    {sel_location: false}
];

const LOCALISATION = [

    {mode: 'eng', number: 1, text: "ENGLISH"},
    {mode: 'hz', number: 2, text: "中文"}
];

const SENTENCE_MODES = [
    {mode: 'hz_py', number: 1, text: "汉语 Chinese"},
    // {mode: 'hz_eng', number: 2, text: "中文-ENGLISH"},
    {mode: 'eng', number: 3, text: "ENGLISH"}
    // {mode: 'hz', number: 4, text: "中文"}
];

const ABOUT = [
    {professor: true},
    {app: false}
]