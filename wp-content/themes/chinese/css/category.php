<?php get_header(); ?>
<div id="category-banner" class="wow fadeInLeft">
	<?php if (function_exists('z_taxonomy_image')) z_taxonomy_image(); ?>
</div>
<div class="benh-list">
	<div class="container">
		<div class="benh-box wow fadeInUp">
			<ul>
				<li class="wow fadeInLeft">
					<a href="<?php echo get_category_link(87); ?>">
						<img src="<?php bloginfo('template_url'); ?>/images/phukhoa.gif" alt="nam-khoa">
						<span>Viêm phụ khoa</span>
					</a>
				</li>
				<li class="wow fadeInLeft">
					<a href="<?php echo get_category_link(93);?>">
						<img src="<?php bloginfo('template_url'); ?>/images/phukhoa.gif" alt="phu-khoa">
						<span>Vô sinh</span>
					</a>
				</li>
				<li class="wow fadeInDown">
					<a href="<?php echo get_category_link(96); ?>">
						<img src="<?php bloginfo('template_url'); ?>/images/phukhoa.gif" alt="da-lieu">
						<span>Kế hoạch hóa gia đình</span>
					</a>
				</li>
				<li class="wow fadeInRight">
					<a href="<?php echo get_category_link(99); ?>">
						<img src="<?php bloginfo('template_url'); ?>/images/phukhoa.gif" alt="hau-mon">
						<span>Bệnh nội tiết</span>
					</a>
				</li>
				<li class="wow fadeInRight">
					<a href="<?php echo get_category_link(102); ?>">
						<img src="<?php bloginfo('template_url'); ?>/images/phukhoa.gif" alt="benh-xa-hoi">
						<span>Bệnh cổ tử cung</span>
					</a>
				</li>
				<li class="wow fadeInRight">
					<a href="<?php echo get_category_link(108); ?>">
						<img src="<?php bloginfo('template_url'); ?>/images/phukhoa.gif" alt="benh-xa-hoi">
						<span>Thẩm mỹ vùng kín</span>
					</a>
				</li>
			</ul>
		</div>
	</div>
</div><!--  end benh list -->
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="category-sidebar wow fadeInLeft">
					<div class="category-sidebar-title">
					</div>
					<?php wp_nav_menu( array( 'theme_location' =>'category-sidebar' ) ); ?>
					<div class="imghome">
						<a target="_blank" href="http://swt.hbhsz.net/LR/Chatpre.aspx?id=lxd68021103&lng=en" ><img src="<?php bloginfo('template_url');?>/images/imghome.jpg"></a>
					</div>
					<?php if ( is_active_sidebar( 'primary-widget-area' ) ) : ?>
<div id="primary" class="widget-area">
<ul class="xoxo">
<?php dynamic_sidebar( 'primary-widget-area' ); ?>
</ul>
</div>
				</div>
<?php endif; ?>
			</div>
			<div class="col-md-9">
				<div class="category-content">
					<div class="category-description wow fadeInRight">
						<h2><span><?php single_cat_title(); ?></span></h2>
						<p><?php echo category_description( $category_id ); ?></p>
						<div class="category-description-tuvan">
							<div class="row">
								<div class="col-md-4">
									<a href="http://swt.hbhsz.net/LR/Chatpre.aspx?id=lxd68021103&lng=en" target="_blank"><img src="<?php bloginfo('template_url');?>/images/tuvantructuyen.jpg"></a>
								</div>
								<div class="col-md-4">
									<a href="http://swt.hbhsz.net/LR/Chatpre.aspx?id=lxd68021103&lng=en" target="_blank"><img src="<?php bloginfo('template_url');?>/images/datlichquamang.jpg"></a>
								</div>
								<div class="col-md-4">
									<a href="http://swt.hbhsz.net/LR/Chatpre.aspx?id=lxd68021103&lng=en" target="_blank"><img src="<?php bloginfo('template_url');?>/images/tuvanquamang.jpg"></a>
								</div>
							</div>
						</div>
						<span class="category-description-line"></span>
					</div>
					<h1 class=" wow fadeInDown"><img src="<?php bloginfo('template_url');?>/images/quality-icon.png"><span>Danh mục <?php single_cat_title(); ?></span></h1>
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<div class="category-box wow zoomIn">
						<a href="<?php the_permalink();?>"><?php the_post_thumbnail('post-image');?></a>
						<div class="post-meta">
							<h2><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
							<?php the_excerpt();?>
						</div>
					</div>
				<?php endwhile; endif; ?>
				<?php wp_pagenavi(); ?>
			</div>
		</div>
	</div>
</div>
</div>
<?php get_footer(); ?>