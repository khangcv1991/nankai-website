<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>"/>
    <meta name="viewport" content="width=device-width"/>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/editor-style.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/video-js.css">
    <link href="http://vjs.zencdn.net/6.2.5/video-js.css" rel="stylesheet">

    <!-- If you'd like to support IE8 -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
    <div id="mobilepart">
        <ul>
            <li><span class="left-show"><i class="fa fa-bars" aria-hidden="true"></i></span></li>
            <li>
                <h1>当我开始学习语言时 <br/> <span>ENglish test</span></h1>
            </li>
            <li><span class="right-show"><i class="fa fa-th-large" aria-hidden="true"></i></span></li>
        </ul>
    </div>
    <div class="se-pre-con"></div>
    <div id="top-header"></div>
    <nav class="navbar navbar-default container">
        <!--        <span class="">-->
        <!--            <ul class="nav navbar-nav">-->
        <!---->
        <!---->
        <!--                <li ng-repeat="item in main_menu track by $index">-->
        <!--                    <a ng-if="localisation.eng" href="{{item.link}}" ng-click="item.action">{{item.eng}}</a>-->
        <!--                    <a ng-if="localisation.hz" href="{{item.link}}">{{item.hz}}</a>-->
        <!--                </li>-->
        <!--            </ul>-->
        <!--        </span>-->
        <div class="radio-group">
            <input type="radio" id="option-one" name="selector" checked><label
                    for="option-one" ng-click="displaySentencesMode(1)"><span ng-if="localisation.eng">汉语 Chinese</span><span
                        ng-if="localisation.hz">汉语</span></label>
            <input type="radio" id="option-three"
                   name="selector" ng-click="displaySentencesMode(3)" ><label for="option-three"><span
                        ng-if="localisation.eng">English 英语</span><span
                        ng-if="localisation.hz">英语</span></label>
        </div>
        <span class="flag">
            <span ng-click="displayContentByLocalisation(1);" style="cursor: pointer;" ng-if="localisation.hz"><img
                        src="<?php bloginfo('template_url'); ?>/images/australia.png"></span>
            <span ng-click="displayContentByLocalisation(2);" style="cursor: pointer;" ng-if="localisation.eng"><img
                        src="<?php bloginfo('template_url'); ?>/images/china.png"></span>
        </span>
    </nav>