<footer id="footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="left-footer">

                    <h4 style="color:red;     font-size: 30px;">{{about_data.Product}}</h4>
                    <ul class="et-social-icons">

                        <li class="et-social-icon et-social-facebook">
                            <a href="#" class="fa fa-facebook"></a>
                        </li>
                        <li class="et-social-icon et-social-twitter">
                            <a href="#" class="fa fa-twitter"></a>
                        </li>
                        <li class="et-social-icon et-social-google-plus">
                            <a href="#" class="fa fa-google-plus"></a>
                        </li>
                        <li class="et-social-icon et-social-rss">
                            <a href="http://www.jinjiyingyu.com/feed/" class="fa fa-rss"></a>
                        </li>

                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                <div class="right-footer" ng-if="about_data">
                    <p>{{about_data.Copyright}}</p>
                    <p>{{about_data.Author}}</p>
                    <p>{{about_data.Feedback}}</p>

                    <p ng-if="version">Content version:{{version.version_no}}</p>
                    <p ng-if="app_build">Release version:{{app_build.version_no}}</p>

                </div>
            </div>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/jquery-ui/jquery-ui.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/angular/angular.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/lib/angular/angular-route.js"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/app/app.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/app/Const.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/app/String.js"></script>
<script type="text/javascript">
    $('.clickmenu').click(function () {
        $('.navbar').toggle(200);
    });
    $(window).load(function () {
        $(".se-pre-con").fadeOut("slow");
        return false;
    });
    if ($(window).width() < 480) {

        $('.navbar').hide();
        $('.iwrapper .aftermobile').after($('.iwrapper #rightsidebar'));

        var language = $('nav.navbar').clone();
        var languagetext = "<span><b>Language</b></span>";
        var iconlang = '<i class="fa fa-caret-down" aria-hidden="true"></i>';
       $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar').prepend(language);
       $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar nav.navbar').prepend(languagetext);
       $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar nav.navbar').prepend(iconlang);
       $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar nav.navbar .radio-group').hide();
       $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar nav.navbar i').click(function(){
        $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar nav.navbar .radio-group').slideToggle();
       });
       $('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar .flag').after($('#leftsidebar > #rightsidebar .col-md-3 .left-sidebar nav.navbar .radio-group'));
        var duration = 'slow';
        $('.left-show').click(function(){
            $("#leftsidebar > .leftsidebar .col-md-3").show("slide", { direction: "left" }, duration);
        });
        $('.right-show').click(function(){
            $("#leftsidebar > #rightsidebar .col-md-3").show("slide", { direction: "right" }, duration);
        });
        $('#leftsidebar > .leftsidebar .col-md-3 > span').click(function(){
            $("#leftsidebar > .leftsidebar .col-md-3").hide();
        });
        $('#leftsidebar > #rightsidebar .col-md-3 > span').click(function(){
            $("#leftsidebar > #rightsidebar .col-md-3").hide();
        });
    }

</script>

</body>
</html>
<?php wp_footer(); ?>