<div ng-app="education" ng-controller="eduController">
    <?php get_header(); ?>
    <div class="clearfix"></div>
    <div class="container">
        <div class="row">
            <div id="leftsidebar">
                <div class="leftsidebar">
                    <div class="col-md-3">
                        <div class="right-sidebar">
                            <div class="radio-group">
                                <input type="radio" id="selector3-one" name="selector3" checked><label
                                for="selector3-one" ng-click="displayAbout(1)"><span
                                ng-if="localisation.eng">PROFESSOR</span><span
                                ng-if="localisation.hz">周教授</span></label>
                                <input type="radio" id="selector3-three"
                                name="selector3" ng-click="displayAbout(2)"><label
                                for="selector3-three"
                                ><span
                                ng-if="localisation.eng">JINJIYINGYU</span><span
                                ng-if="localisation.hz">紧急英语</span></label>
                            </div>

                            <ul>
                                <li ng-if="about_navigation.professor">
                                    <h5 style="text-align: center;text-decoration: underline;font-weight: 600;"
                                    ng-if="flag.eng">
                                    ABOUT PROFESSOR</h5>
                                    <h5 ng-if="flag.hz_py"
                                    style="text-align: center;text-decoration: underline;font-weight: 600;">
                                    周教授</h5>

                                    <div class="circle-image"><img
                                        src="<?php bloginfo('template_url'); ?>/images/Zhou2.png">
                                    </div>
                                    <p style="text-align: center;font-size: smaller;" ng-if="flag.hz_py">
                                        当我开始学习语言时，我总是用孩童般的眼光去学习它。孩子们的书籍以及学习资料都十分基础，还分成了很多的小单元——如果你时间紧张，这会比你翻一本厚厚的笔记本简单多了。</p>

                                        <p ng-if="flag.eng">When I start to learn languages, I often approach it
                                            through the eyes of a
                                            child. Children’s books and learning materials start with the basics and
                                            break them down into small fragments—and when you’re pressed for time, that
                                            can be much easier than getting into a dense workbook.</p>
                                        </li>
                                        <li ng-if="about_navigation.app">
                                            <h5 style="text-align: center;text-decoration: underline;font-weight: 600;">
                                                <span ng-if="flag.eng">ABOUT JINJIYINGYU </span> <span
                                                ng-if="flag.hz_py">紧急英语 </span> - <span ng-if="version"> version {{version.version_no}}</span>
                                            </h5>
                                            <div>
                                                <img src="<?php bloginfo('template_url'); ?>/images/e-learning.jpeg">
                                            </div>
                                            <p style="text-align: center;font-size: smaller;" ng-if="flag.hz_py">
                                                紧急英语是一款可以帮助你在海外生存的实用学习软件</p>
                                                <p ng-if="flag.eng">Prof Zhou's JinjiYingYu is practical advice on how
                                                    to
                                                    survive your overseas
                                                    studies.</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <span></span>
                                    </div>
                                </div>

                                <div ng-view></div>
                            </div>
                        </div>
                    </div>
                    <span class="aftermobile"></span>
                    <div id="rightsidebar">

                        <div class="col-md-3">
                            <div class="left-sidebar">
                                <div class="radio-group">
                                    <input type="radio" id="selector2-one" name="selector2" checked><label
                                            for="selector2-one" ng-click="displayCategory_Location(1)"><span
                                                ng-if="localisation.eng">CATEGORY</span><span
                                                ng-if="localisation.hz">类别</span></label>
                                    <input type="radio" id="selector2-three"
                                           name="selector2" ng-click="displayCategory_Location(2)"><label
                                            for="selector2-three"
                                    ><span
                                                ng-if="localisation.eng">LOCATION</span><span
                                                ng-if="localisation.hz">地点</span></label>
                                </div>

                                <a href="#scene" ng-click="listScenes()"><p>EVERYTHING</p></a>
                                <ul ng-if="category_location.sel_location">
                                    <li ng-repeat="item in locations track by $index"
                                        ng-click="listScenesByLocationId(item.id);">
                                        <a href="#scene" id="{{scene.id}}">
                                            <img src="<?php bloginfo('template_url'); ?>/images/book.png">
                                            <div class="category-meta">

                                                <h4 ng-if="flag.eng">
                                                    {{item.nameTranslations.en}}
                                                    <span ng-if="item.nameTranslations.en == null || item.nameTranslations.en ==''">
                                                    N/A
                                                </span>
                                                </h4>
                                                <h4 ng-if="flag.hz_py">{{item.nameTranslations.zh_hz}}
                                                    <span ng-if="item.nameTranslations.zh_hz == null || item.nameTranslations.zh_hz ==''">
                                                    不值
                                                </span>
                                                </h4>

                                <div class="col-md-6">
                                    <div id="education">
                                        <div class="education-controller">
                                            <div class="breadcrumb">
                                                <div class="breadcrumb_step1 scenestodo" ng-show="flag.scene">
                                                    <a ng-if="localisation.eng" href="#scene" ng-click="listScenes()">Scene <span
                                                        ng-if="selScene.sequence">{{selScene.sequence}} </span></a>
                                                        <a ng-if="localisation.hz" href="#scene" ng-click="listScenes()">场景 <span
                                                            ng-if="selScene.sequence">{{selScene.sequence}} </span></a>
                                                        </div>
                                                        <div class="breadcrumb_step2 arrow" style="color: red;" ng-show="flag.dialog">►
                                                        </div>
                                                        <div class="breadcrumb_step2 dialogtodo" ng-show="flag.dialog">
                                                            <a ng-if="localisation.eng" href="#dialog" ng-click="listDialogsById(selScene)">Dialog
                                                                <span ng-if="selDialog.sequence">{{selDialog.sequence}} </span></a>
                                                                <a ng-if="localisation.hz" href="#dialog"
                                                                ng-click="listDialogsById(selScene)">对话 <span ng-if="selDialog.sequence">{{selDialog.sequence}} </span></a>
                                                            </div>

                                                        </div>
                                                        <div ng-view></div>
                                                    </div>
                                                </div>

                                            </div>
                                            <span class="aftermobile"></span>
                                            <div id="rightsidebar">

                                                <div class="col-md-3">
                                                    <div class="left-sidebar">
                                                        <div class="radio-group">
                                                            <input type="radio" id="selector2-one" name="selector2" checked><label
                                                            for="selector2-one" ng-click="displayCategory_Location(1)"><span
                                                            ng-if="localisation.eng">CATEGORY</span><span
                                                            ng-if="localisation.hz">类别</span></label>
                                                            <input type="radio" id="selector2-three"
                                                            name="selector2" ng-click="displayCategory_Location(2)"><label
                                                            for="selector2-three"
                                                            ><span
                                                            ng-if="localisation.eng">LOCATION</span><span
                                                            ng-if="localisation.hz">地点</span></label>
                                                        </div>

                                                        <a href="#scene" ng-click="listScenes()"><p>EVERYTHING</p></a>
                                                        <ul ng-if="category_location.sel_location">
                                                            <li ng-repeat="item in locations track by $index" ng-click="listScenesByLocationId(item.id)">
                                                                <a href="#scene" id="{{scene.id}}">
                                                                    <img src="<?php bloginfo('template_url'); ?>/images/book.png">
                                                                    <div class="category-meta">

                                                                        <h4 ng-if="flag.eng">
                                                                            {{item.titleTranslations.en}}
                                                                            <span ng-if="item.titleTranslations.en == null || item.titleTranslations.en ==''">
                                                                                N/A
                                                                            </span>
                                                                        </h4>
                                                                        <h4 ng-if="flag.hz_py">{{item.titleTranslations.zh_hz}}
                                                                            <span ng-if="item.titleTranslations.zh_hz == null || item.titleTranslations.zh_hz ==''">
                                                                                不值
                                                                            </span>
                                                                        </h4>

                                                                    </div>
                                                                </a>
                                                            </li>

                                                        </ul>


                                                        <ul ng-if="category_location.sel_category">
                                                            <li ng-repeat="item in categories track by $index">
                                                                <a href="#scene" id="{{scene.id}}"
                                                                ng-click="listScenesByCategoryId(item.id);">
                                                                <img src="<?php bloginfo('template_url'); ?>/images/book.png">
                                                                <div class="category-meta">

                                                                    <h4 ng-if="flag.eng">
                                                                        {{item.titleTranslations.en}}
                                                                        <span ng-if="item.titleTranslations.en == null || item.titleTranslations.en ==''">
                                                                            N/A
                                                                        </span>
                                                                    </h4>
                                                                    <h4 ng-if="flag.hz_py">{{item.titleTranslations.zh_hz}}
                                                                        <span ng-if="item.titleTranslations.zh_hz == null || item.titleTranslations.zh_hz ==''">
                                                                            不值
                                                                        </span>
                                                                    </h4>

                                                                </div>
                                                            </a>
                                                        </li>

                                                    </ul>
                                                    <!--                            <h2>Locations</h2>-->
                                                    <!--                            <p style="text-align:center;">Everything</p>-->
                                                </div>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php get_footer(); ?>
                        </div>
