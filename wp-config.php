<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress2');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
define('WPCACHEHOME','/var/www/html/wp-content/plugins/wp-super-cache/');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'B+b]C6AR+9/Uo(jO@^0O;%AH,!E^Wn%xF(KI]yKxe?{2]Isse_Q1u1+Nc7kq}j:F');
define('SECURE_AUTH_KEY',  'P<``e)[1(fB5GcNt*(`jF`5`g;hA@r0=Bqv$Tcyr0+&N@X>$|55qb1rTvz 65uVN');
define('LOGGED_IN_KEY',    'n!2^?OOUF1({Da%-X;MARfh/?VK{1ikum0hT:N:6@w&BW/v{kuR31pN|QFX3$B1w');
define('NONCE_KEY',        '@gt-f$*&apx8.]LP_W|eAlcc`kIS+l?HFYG#v::.;JYI#y{9RZ##t-$@ZmB2a9h*');
define('AUTH_SALT',        'ZvQ}YG2;8j1x-:147+L7)35T^Un*pHxYkV@@)eE:~/*2b@?CuHEsrLZnUB!PrTo=');
define('SECURE_AUTH_SALT', 'K]jZJZ$|q%GXR{HNqg=0<, nKBwtS<zBp]-%9Pz<@JmU]D!)5c-zo+HUyzpm;V&!');
define('LOGGED_IN_SALT',   '.gENE8L~${M4Y8gZ,K-9v7S+YGx(e6~Y=@8Dy; Q_Ps_o|_m?..n9.2?$Hk&OL!5');
define('NONCE_SALT',       '|dUpR)d{b,,y)?H_YvFeyuX0d:S=Q,ziHsxF|6-oq:Ag,+-f2XhlK>2A? Ir]>Lb');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
